Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  root to: 'events#index'
  resources :events
  put 'complete_event', to: 'events#complete'
end
