class AddUserIdForEvents < ActiveRecord::Migration[5.2]
  def up
    add_column :events, :user_id, :integer
    add_foreign_key :events, :users, column: :user_id, name: :fk_rails_59722bd974
  end

  def down
    remove_foreign_key :events, name: :fk_rails_59722bd974
    remove_column :events, :user_id
  end
end
