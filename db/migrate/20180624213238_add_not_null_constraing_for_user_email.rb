class AddNotNullConstraingForUserEmail < ActiveRecord::Migration[5.2]
  def up
    change_column_null :users, :email, false
    change_column_default :users, :email, ''
  end

  def down
    change_column_default :users, :email, nil
    change_column_null :users, :email, nil
  end
end
