class AddEventsTable < ActiveRecord::Migration[5.2]
  def change
    create_table 'events' do |t|
      t.string :title, null: false
      t.string :tags
      t.timestamp :scheduled_date, null: false
    end
  end
end
