require 'rails_helper'

RSpec.describe 'Sign in', type: :feature, js: true do
  let!(:user) { User.create!(email: 'test@mail.com', password: '123456') }

  before do
    visit root_path
  end

  context 'successful' do
    subject { click_button 'Log in' }

    let(:user_email) { user.email }
    let(:user_password) { user.password }

    before do
      fill_in 'Email', with: user_email
      fill_in 'user_password', with: user_password
    end

    it 'should create new user' do
      expect { subject }.to change { user.reload.sign_in_count }.by(1)
      expect(page).to have_content 'Signed in successfully.'
    end
  end

end
