require 'rails_helper'

RSpec.describe 'search', type: :feature, js: true do
  let(:user) { User.create!(email: 'test@mail.com', password: '123456') }
  let!(:search_event) { user.events.create(title: 'my super_event', scheduled_date: DateTime.now.utc) }
  let!(:out_of_scope_event) { user.events.create(title: 'outdated event', scheduled_date: DateTime.now.utc) }

  # todo user Warden login instead
  before do
    visit root_path
    fill_in 'Email', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Log in'
  end

  context 'successful' do
    subject { click_on 'Search' }

    before do
      fill_in 'term', with: 'super_event'
    end

    it 'should filter events' do
      subject
      within 'table.table' do
        expect(page).to have_content(search_event.title)
        expect(page).not_to have_content(out_of_scope_event.title)
      end
    end
  end
end
