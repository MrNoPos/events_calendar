require 'rails_helper'

RSpec.describe 'Sign up', type: :feature, js: true do
  before do
    visit new_user_registration_path
  end

  context 'successful' do
    subject { click_button 'Sign up' }

    let(:user_email) { 'kirill.s@mail.com' }
    let(:user_password) { '123456' }

    before do
      fill_in 'Email', with: user_email
      fill_in 'user_password', with: user_password
      fill_in 'user_password_confirmation', with: user_password
    end

    it 'should create new user' do
      expect { subject }.to change { User.count }.by(1)
      expect(page).to have_content 'Welcome! You have signed up successfully.'
    end
  end

end
