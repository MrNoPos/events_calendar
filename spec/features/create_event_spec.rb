require 'rails_helper'

RSpec.describe 'create event', type: :feature, js: true do
  let!(:user) { User.create!(email: 'test@mail.com', password: '123456') }

  # todo user Warden login instead
  before do
    visit root_path
    fill_in 'Email', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Log in'
    click_on 'New Event'
  end

  context 'successful' do
    subject { click_on 'Create Event' }

    it 'should create new event' do
      fill_in 'event_title', with: 'some_title'
      fill_in 'event_tags', with: 'tags'
      expect { subject }.to change { user.events.count }.by(1)
    end
  end
end
