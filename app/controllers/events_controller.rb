class EventsController < ApplicationController
  before_action :authenticate_user!

  def new
    @event = current_user.events.new
  end

  def create
    @event = current_user.events.create(event_params)
    redirect_to events_path
  end

  def edit
    @event = Event.find(params[:id])
  end

  def index
    @events = if search_params[:term]
                current_user.events.where('title LIKE ?', "%#{search_params[:term]}%")
              else
                current_user.events
              end
  end

  def update
    @event = Event.find(params[:id])
    if @event.update(event_params)
      redirect_to events_path
    else
      render 'edit'
    end
  end

  def destroy
    Event.find(params[:id]).destroy
    redirect_to events_path
  end

  def complete
    current_user.events.find(params[:id]).update!(completed: true)
    redirect_to events_path
  end

  private

  def search_params
    params.permit(:term)
  end

  def event_params
    params.require(:event).permit(:title, :scheduled_date, :tags, :term)
  end
end
