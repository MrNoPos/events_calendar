# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string
#  password               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2]
  self.table_name = 'users'

  has_many :events, foreign_key: :user_id, class_name: 'Event'

  validates_presence_of :email, :password

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first
    user = User.create(email: data['email'], password: Devise.friendly_token[0,20]) unless user
    user
  end
end
