# == Schema Information
#
# Table name: events
#
#  id             :bigint(8)        not null, primary key
#  title          :string           not null
#  tags           :string
#  scheduled_date :datetime         not null
#  user_id        :integer
#  completed      :boolean          default(FALSE), not null
#

class Event < ActiveRecord::Base
  self.table_name = 'events'

  validates_presence_of :title, :scheduled_date
  validates_inclusion_of :completed, in: [true, false]

  belongs_to :user, foreign_key: :user_id, class_name: 'User'
end
